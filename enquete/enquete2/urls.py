from django.urls import path
from . import views
from django.conf import settings
from django.conf.urls.static import static

app_name = 'enquete2'
urlpatterns = [

        path('index',views.index,name='index'),
        path('about',views.about,name='about'),
        path('contact',views.contact,name='contact'),
        path('topico/<int:topico_id>',views.topico,name='topico'),

] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)

