from django.contrib import admin
from .models import Categoria, Topico, Opcao

# Register your models here.
admin.site.register(Categoria)
admin.site.register(Topico)
admin.site.register(Opcao)
