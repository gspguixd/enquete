from django.shortcuts import render
from django.views import generic
from .models import Topico

def index(request):
    topico_id = Topico.objects.get(id=1)
    return render(request, 'enquete2/index.html', {'topico': topico_id})

def about(request):
    return render(request, 'enquete2/about.html', {})

def contact(request):
    return render(request, 'enquete2/contact.html', {})

def topico(request, topico_id):
    topico = Topico.objects.get(id=topico_id)
    topico_single = Topico.objects.all()
    return render(request, 'enquete2/single.html', {'topico': topico_single})




