from django.db import models
from django.utils import timezone
import datetime

class Categoria(models.Model):
    titulo = models.CharField(max_length = 200)
    data_publicacao = models.DateTimeField('Data de publicação')
    def __str__(self):
        return self.titulo

class Topico(models.Model):
    texto = models.CharField(max_length = 200)
    descricao = models.CharField(max_length = 500)
    data_publicacao = models.DateTimeField('Data de publicação')
    categoria = models.ManyToManyField(Categoria)
    def __str__(self):
        return self.texto
    def publicada_recentemente(self):
        agora = timezone.now()
        return agora-datetime.timedelta(days=1) <= self.data_publicacao <= agora

class Opcao(models.Model):
    texto = models.CharField(max_length = 200)
    votos = models.IntegerField(default = 0)
    topico = models.ForeignKey(Topico, on_delete = models.CASCADE)
    def __str__(self):
        return self.texto