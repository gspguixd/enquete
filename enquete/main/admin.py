from django.contrib import admin
from .models import Pergunta, Opcao, Categoria

# Register your models here.
admin.site.register(Pergunta)
admin.site.register(Opcao)
admin.site.register(Categoria)
