from django.urls import path
from . import views

app_name = 'main'

urlpatterns = [
        path('',
        views.root,
        name='root'),

        path('enquete',
        views.IndexViews.as_view(),
        name='index'),

        path('pergunta/<int:pk>',
        views.DetalhesViews.as_view(),
        name ='detalhes'),

        path('pergunta/<int:pk>/resultado',
        views.ResultadoViews.as_view(),
        name ='resultado'),

        path('pergunta/<int:id_enquete>/votacao',
        views.votacao,
        name = 'votacao'),

        path('pergunta/<int:id_categoria>/categoria',
        views.categoria,
        name = 'categoria'),


    ]

