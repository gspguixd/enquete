from django.shortcuts import render, get_object_or_404
from django.http import HttpResponseRedirect
from .models import Pergunta, Opcao, Categoria
from django.urls import reverse
from django.views import generic
from django.utils import timezone

def root(request):
    return render(request, 'main/root.html')

class IndexViews(generic.ListView):
    template_name = 'main/index.html'
    context_object_name = 'ultimas_perguntas'
    def get_queryset(self):
        return Pergunta.objects.filter(
            data_publicacao__lte=timezone.now()
        ).order_by('-data_publicacao')[:5]

class DetalhesViews(generic.DetailView):
    model = Pergunta
    template_name = 'main/detalhes.html'
    def get_queryset(self):
        return Pergunta.objects.filter(data_publicacao__lte=timezone.now())

class ResultadoViews(generic.DetailView):
    model = Pergunta
    template_name = 'main/resultado.html'

def categoria(request, id_categoria):
    context = get_object_or_404(Categoria, pk=id_categoria)
    pergunta = context.pergunta_set.all().order_by('texto')
    return render(request, 'main/categoria.html', {'categoria': context, 'pergunta':pergunta })

def votacao(request, id_enquete):
    pergunta = get_object_or_404(Pergunta, pk=id_enquete)
    try:
        opcao_selecionada = pergunta.opcao_set.get(pk=request.POST['opcao'])
    except (KeyError, Opcao.DoesNotExist):
        return render(request, 'main/detalhes.html', {
            'pergunta':pergunta,
            'error_message': "Selecione uma opção VÁLIDA!",
            })
    else:
        opcao_selecionada.votos += 1
        opcao_selecionada.save()
        return HttpResponseRedirect(reverse('main:resultado', args=(pergunta.id,)))
